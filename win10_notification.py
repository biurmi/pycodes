# Simple Windows 10 notifier using win10toast
from win10toast import ToastNotifier


def win_popup(title: str, message: str, duration: int = 5):
    toast = ToastNotifier()
    toast.show_toast(title, message, duration=duration)
