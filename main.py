from sorters import bubbly_sorter as bs, quickly_sorter as qs, inserty_sorter as iso


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    to_sort = [6, 43, 5, -1, -2, -43, 3, 0, 90]

    print("Bubble    sort : {0}".format(bs.bubble_xor_sort(to_sort)))
    print("Quick     sort : {0}".format(qs.quick_sort_recursive(to_sort)))
    print("Insertion sort : {0}".format(iso.insertion_sort(to_sort)))
    # toaster.win_popup("Sorted list", str(bs.bubble_xor_sort(to_sort)))

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
