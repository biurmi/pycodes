# Bubble sort algorithm that returns sorted list.


def bubble_sort(list_to_sort: list):
    if len(list_to_sort) == 0:
        return "Error: list is empty."
    for i in range(len(list_to_sort) - 1, 0, -1):
        for x in range(i):
            if list_to_sort[x] > list_to_sort[x + 1]:
                holder = list_to_sort[x]
                list_to_sort[x] = list_to_sort[x + 1]
                list_to_sort[x + 1] = holder
    return list_to_sort


def bubble_xor_sort(list_to_sort: list):
    if len(list_to_sort) == 0:
        return "Error: list is empty."
    for i in range(len(list_to_sort) - 1, 0, -1):
        for x in range(i):
            if list_to_sort[x] > list_to_sort[x + 1]:
                list_to_sort[x] = list_to_sort[x] ^ list_to_sort[x + 1]
                list_to_sort[x + 1] = list_to_sort[x + 1] ^ list_to_sort[x]
                list_to_sort[x] = list_to_sort[x] ^ list_to_sort[x + 1]
    return list_to_sort
