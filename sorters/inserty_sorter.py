# Insertion sort algorithm
def insertion_sort(lts: list):
    for i in range(1, len(lts)):
        current = lts[i]
        position = i

        while position > 0 and lts[position - 1] > current:
            lts[position] = lts[position - 1]
            position -= 1
        lts[position] = current
    return lts
