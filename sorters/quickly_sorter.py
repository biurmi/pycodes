# Quick sort algorithm that returns sorted list
def quick_sort_recursive(lts: list):          # lts - list to sort
    if len(lts) <= 1:
        return lts
    else:
        rotor = lts[len(lts) // 2]
        left_side = [x for x in lts if x < rotor]
        break_point = [x for x in lts if x == rotor]
        right_side = [x for x in lts if x > rotor]
        return quick_sort_recursive(left_side) + break_point + quick_sort_recursive(right_side)

